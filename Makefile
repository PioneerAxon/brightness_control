CC=gcc

all:brightness_ctl
	sudo chown root:root brightness_ctl
	sudo chmod 4755 brightness_ctl

brightness_ctl:brightness.c
	$(CC) brightness.c -o brightness_ctl

clean:
	rm *.o brightness_ctl
