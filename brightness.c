#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

#define BRIGHTNESS_FILE "/sys/class/backlight/intel_backlight/brightness"
#define MAX_BRIGHTNESS_FILE "/sys/class/backlight/intel_backlight/max_brightness"
#define steps 64

void write_file (const char* name, int value)
{
	FILE* fp;
	fp = fopen (name, "w");
	assert (fp != NULL);
	fprintf (fp, "%d", value);
	fclose (fp);
}

int read_file (const char* name)
{
	FILE* fp;
	int ret;
	fp = fopen (name, "r");
	assert (fp != NULL);
	fscanf (fp, "%d", &ret);
	fclose (fp);
	return ret;
}

int main (int argc, char** argv)
{
	assert (argc == 2);
	assert (setuid (0) == 0);
	int max, cur;
	max = read_file (MAX_BRIGHTNESS_FILE);
	cur = read_file (BRIGHTNESS_FILE);
	if (argv [1][0] == 'i')
	{
		cur += max / steps;
		if (cur > max)
			cur = max;
		write_file (BRIGHTNESS_FILE, cur);
	}
	else if (argv [1][0] == 'd')
	{
		cur -= max / steps;
		if (cur < 0)
			cur = 0;
		write_file (BRIGHTNESS_FILE, cur);
	}
	else
	{
		assert (0);
	}
	return 0;
}
